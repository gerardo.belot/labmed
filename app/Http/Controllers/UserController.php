<?php

namespace App\Http\Controllers;

use Acme\Helpers\Miselanius;
use Acme\Helpers\UsersControllerHelper;
use App\Http\Requests\UserRequest;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkActive');
        $this->middleware('ManageUsers')->except('index');
        $this->middleware('SeeUsers')->only('index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $items = User::all();
        return View('seguridad.usuarios.index', compact('items'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles = Role::pluck('display_name', 'id');
        return View('seguridad.usuarios.create', compact('roles'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $helper = new Miselanius();
        $userHelper = new UsersControllerHelper();

        if ($request->input('password')):
            $request['password'] = bcrypt($request->input('password'));
            unset($request['password_confirmation']);
        else:
            unset($request['password']);
            unset($request['password_confirmation']);
        endif;

        $request['status'] = $helper->checkRequestStatus($request);
        $userHelper->storeAndSync($request);
        return redirect()->to('/usuarios');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $item = User::findOrfail($id);
        $roles = Role::pluck('display_name', 'id');
        return View('seguridad.usuarios.edit', compact('item', 'roles'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $helper = new Miselanius();
        $userHelper = new UsersControllerHelper();

        if ($request->input('password')):
            $request['password'] = bcrypt($request->input('password'));
            unset($request['password_confirmation']);
        else:
            unset($request['password']);
            unset($request['password_confirmation']);
        endif;

        $request['status'] = $helper->checkRequestStatus($request);

        $userHelper->UpdateAndSync($request, $user);
        return redirect()->to('/usuarios');
    }

    /**
     * @param $id
     * @param $state
     * @return mixed
     */
    public function state($id, $state){
        $item = User::findOrFail($id);
        $item->status = $state;
        $item->update();
        return $item->name;
    }

}
